﻿using GraphQL;
using GraphQL.Types;

namespace fatweekday.API.Model
{
    public class FatweekdaySchema : Schema
	{
		public FatweekdaySchema(IDependencyResolver resolver)
			: base(resolver)
		{
			Query = resolver.Resolve<FatweekdayQuery>();
			Mutation = resolver.Resolve<FatweekdayMutation>();
		}
	}
}
