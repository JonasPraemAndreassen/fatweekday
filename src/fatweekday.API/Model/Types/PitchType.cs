﻿using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model.Types
{
    public class PitchType : ObjectGraphType<Pitch>
	{
		public PitchType() {
			Name = "Pitch";

			Field(h => h.Id).Description("The Pitch Id");
			Field(h => h.Name).Description("The Pitch name");
			Field(h => h.Description, nullable: true).Description("The Pitch description");
			Field(h => h.Effort).Description("The Pitch effort");

			Field<ListGraphType<LikeType>>(
				"Likes",
				resolve: context => context.Source.Likes
			);

			Field<IntGraphType>(
				"LikeCount",
				resolve: context => context.Source.Likes.Count()
			);
		}
    }
}
