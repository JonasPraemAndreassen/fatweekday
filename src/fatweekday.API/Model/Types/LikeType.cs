﻿using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model.Types
{
    public class LikeType : ObjectGraphType<Like>
	{
		public LikeType() {
			Name = "Like";

			Field(h => h.PitchId).Description("The Pitch Id");
			Field(h => h.UserId).Description("The User Id");
		}
    }
}
