﻿using fatweekday.API.Model.Types;
using fatweekday.Domain.Interfaces;
using fatweekday.Domain.Models;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fatweekday.API.Model
{
	public class FatweekdayQuery : ObjectGraphType<object>
	{
		public FatweekdayQuery(IPitchRepository repo)
		{
			Name = "Query";

			Field<PitchType>(
				"pitch",
				arguments: new QueryArguments(
					new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "id", Description = "id of the human" }
				),
				resolve: context => repo.GetById(context.GetArgument<int>("id"))
			);

			Field<ListGraphType<PitchType>>(
				"pitches",
				resolve: context => repo.GetAll()
			);
		}
	}
}
