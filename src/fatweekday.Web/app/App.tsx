﻿import * as React from 'react';
import PitchList from './PitchList';
import './styles/styles.scss';

class App extends React.Component<any, any> {

    constructor(props: any){
        super(props);
        this.state = { ideas: ['fatweekday portal', 'Impact Slackbot', '3rd idea']};
    }

    public render() {
        return (
            <div className="app">
                    <PitchList/>
            </div>
        );
    }
}

export default App;