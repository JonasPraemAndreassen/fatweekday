import * as React from 'react';
import * as ReactDOM from 'react-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import App from './App';

const client = new ApolloClient({ uri: (window as any)["graphql_endpoint"] });

ReactDOM.render(
	<ApolloProvider client={client}>
		<div id="navigation-header">fatweekday</div>
		<App />
	</ApolloProvider>,
	document.getElementById('root') as HTMLElement
);