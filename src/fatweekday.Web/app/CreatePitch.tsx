import * as React from 'react';
import { gql } from 'apollo-boost';
import { Mutation } from 'react-apollo';

const CREATE_PITCH = gql`
    mutation CreatePitch($pitch: PitchInput!) {
        createPitch(pitch: $pitch) {
            id
        }
    }
`

export interface Props {
    onUpdate: any;
}

const createPichComponent: React.SFC<Props> = (props) => {
    return (
        <Mutation mutation={CREATE_PITCH}>{(createPitch, { data }) => (
            <div className="create-container">
                <form
                    onSubmit={e => {
                        e.preventDefault();
                        createPitch({
                            variables: {
                                pitch: {
                                    "name": "534543",
                                    "effort": 21
                                }
                            }
                        });
                        console.log('added');
                        props.onUpdate();
                    }}
                >
                    <input className="input-field input-field_name" placeholder="pitch"></input>
                    <input className="input-field input-field_effort" placeholder="effort"></input>
                    <button className="button-submit" type="submit"><div>+</div></button>
                </form>
            </div>
        )}
        </Mutation>
    );
}
// export interface Props {
//     createPitch?: any;
// }

export default createPichComponent;