﻿import * as React from 'react';

interface IPitchItemProps {
	id: number;
	name: string;
	effort: number;
	likes: number;
}

class App extends React.Component<IPitchItemProps, any> {

	constructor(props: IPitchItemProps) {
		super(props);
	}

	public render() {
		return (
			<div className="pitch-container">
				{this.props.name}
				<div className="effort">effort: {this.props.effort}</div>
				<div className="likes">{this.props.likes}</div>
			</div>
		);
	}
}

export default App;