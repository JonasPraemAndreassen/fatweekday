import * as React from 'react';
import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';
import CreatePitchComponent from './CreatePitch';
import PitchItem from './PitchItem';

const GET_PITCH = gql`
	{
		pitches {
			id,
			name,
			effort,
			likeCount
		}
	}
`

const pitchList = () => (
    <Query
        query={GET_PITCH}
        pollInterval={500}
    >
        {({ loading, error, data, refetch }) => {
            if (loading) return <p>Loading...</p>;
            if (error) return <p>Error :(</p>;

            return (
                <div>
                    <CreatePitchComponent
                        onUpdate={() => refetch()}

                    />
                    <div className="list-container">
                        {data.pitches.map((pitch: any, index: number, ) => {
                            return <PitchItem id={pitch.id} name={pitch.name} likes={pitch.likeCount} effort={pitch.effort} />
                        })}
                    </div>
                </div>
            );
        }}
    </Query>
);

export default pitchList;