const merge = require('webpack-merge');
const base = require('./webpack.config.js')();

module.exports = (env) => {
	return [merge(base, {
		mode: 'production',
		devtool: 'false'
	})];
};