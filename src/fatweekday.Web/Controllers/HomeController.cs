﻿using fatweekday.Web.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace fatweekday.Web.Controllers
{
	public class HomeController : Controller
	{
		AppSettings appSettings { get; }

		public HomeController(AppSettings appSettings)
		{
			this.appSettings = appSettings;
		}

		public IActionResult Index()
		{
			ViewData["GraphQL:Endpoint"] = appSettings.GraphQLEndpoint;

			return View();
		}
	}
}