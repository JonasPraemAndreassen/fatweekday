const path = require('path');
const webpack = require('webpack');

const bundleOutputDir = './wwwroot/dist';

module.exports = (env) => {
	return {
		mode: 'development',
		devtool: 'source-map',

		stats: { modules: false },
		entry: {
			vendor: ['react', 'react-dom', 'apollo-boost', 'react-apollo']
		},
		resolve: { extensions: ['.js'] },
		output: {
			path: path.join(__dirname, bundleOutputDir),
			filename: '[name].js',
			publicPath: 'dist/',
			library: '[name]_[hash]'
		},
		module: {
			rules: []
		},
		plugins: [
			new webpack.DllPlugin({
				path: path.join(__dirname, 'wwwroot', 'dist', '[name]-manifest.json'),
				name: '[name]_[hash]'
			}),
		]
	};
};